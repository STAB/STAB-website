---
title: "STAB groups"
author: STAB Coordinators
date: '2024-09'
draft: false
categories: []
tags: []
---

In 2024, three Societal Transition And Behavior change groups were formed: the Circular-STAB group (C-STAB), the Just Transitions group, and the Sustainable Transitions, Equity, and Behavior group (STEB).

The [C-STAB group](../groups/C-STAB.qmd) is united by the mission is to identify enablers of the circular economy at the individual level (citizens, consumers, employees) and the collective level (teams, families, organizations), as well their connection with system-level changes (policies, practices). So this end, the group studies psychosocial enablers of the circular economy and explores the repercussions of behavioral changes on complex social systems (ranging from small family units to large business networks or geographic regions).

The [Social Transitions group](../groups/ssocial-transitions.qmd) has the goal to inform the development of policies/interventions to reduce inequality. To this end, they focus on the (unintended) consequences that different transitions have on social and economic inequalities, aiming to investigate the mechanisms that lead to, and perpetuate, inequality. They will develop an analytical tool to map the ways in which transition processes produce increasing—or decreasing—inequality.

The [STEB group](../groups/STEB.qmd) for now remains the most mysterious group: they will reveal their goals and plans on the tenth of October 2024!


