---
title: Fourth National STAB Meeting
author: STAB Coordinators
draft: true
execute:
  echo: false
  results: asis
  freeze: false
editor_options: 
  chunk_output_type: console
params:
  gs_id: "https://docs.google.com/document/d/19ja1mwEBt9jtLrCEtr5srhPJ9-k2JPcDfdw0oY5-ipA/edit?tab=t.0"
---

```{r}
#| results: asis
gs_id <-
  gsub(
    ".*/document/d/(.*)/.+",
    "\\1",
    params$gs_id
  );
cat(
  readLines(
    paste0(
      "https://docs.google.com/document/d/",
      gs_id,
      "/export?format=txt"
    ),
    warn = FALSE
  ),
  sep = "\n"
)
```
