---
title: Roy Kemmers
description: ""
execute:
  echo: false
  results: asis
  freeze: false
editor_options: 
  chunk_output_type: console
params:
  gs_id: https://docs.google.com/document/d/1neYg3lSBUhms1fZaI4MXesPdXX9Vfgg3c--8Ff46E8Q/edit?usp=sharing
---

```{r}
#| results: asis
gs_id <-
  gsub(
    ".*/document/d/(.*)/.+",
    "\\1",
    params$gs_id
  );
cat(
  readLines(
    paste0(
      "https://docs.google.com/document/d/",
      gs_id,
      "/export?format=txt"
    ),
    warn = FALSE
  ),
  sep = "\n"
)
```
